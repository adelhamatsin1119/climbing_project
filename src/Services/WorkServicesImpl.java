package Services;

import Models.Model;
import Models.ObjectsWork;
import Models.Workers;
import Models.WorksDays;
import Repositories.*;

public class WorkServicesImpl implements WorkServices {
    private WorkersRepositories workersRepositories;
    private WorkObjectsRepositories workObjectsRepositories;
    private WorksDaysRepositories worksDaysRepositories;

    public WorkServicesImpl(WorkersRepositories workersRepositories, WorkObjectsRepositories workObjectsRepositories
            , WorksDaysRepositories worksDaysRepositories) {
        this.workObjectsRepositories = workObjectsRepositories;
        this.workersRepositories = workersRepositories;
        this.worksDaysRepositories = worksDaysRepositories;
    }

    @Override
    public void addingEntry(String fullName, String date, String nameObject, int workTime, String typeOfWork) {
        Workers worker = getWorker(fullName);
        ObjectsWork objectWork = getObjectWork(nameObject);
        WorksDays workDay = getWorkDay(date, nameObject, fullName);

        worker.setWorkerObject(objectWork);
        worker.setWorkingHours(workTime);

        objectWork.setWorker(worker);

        workDay.setWorker(worker);
        workDay.setObjectsWork(objectWork);
        workDay.setHoursWorked(workTime);
        workDay.setTypeOfWork(typeOfWork);

        UpdateOrSave(worker,workersRepositories);
        UpdateOrSave(objectWork,workObjectsRepositories);
        UpdateOrSave(workDay,worksDaysRepositories);

    }

    private WorksDays getWorkDay(String date, String nameObject, String fullName) {
        String infoDay = date + "|" + nameObject + "|" + fullName;
        if (worksDaysRepositories.containsByName(infoDay)) {
            return worksDaysRepositories.findByName(infoDay);
        } else {
            return new WorksDays(date);
        }
    }


    private ObjectsWork getObjectWork(String nameObject) {

        if (workObjectsRepositories.containsByName(nameObject)) {
            return workObjectsRepositories.findByName(nameObject);
        } else {
            return new ObjectsWork(nameObject);
        }
    }

    private Workers getWorker (String fullName) {

        if (workersRepositories.containsByName(fullName)) {
           return workersRepositories.findByName(fullName);
        } else {
            return new Workers(fullName);
        }
    }

    private void UpdateOrSave(Model model, Repositories repositories) {
        if (repositories.containsByName(model.getId())) {
            repositories.update(model);
        } else {
            repositories.save(model);
        }
    }
}
