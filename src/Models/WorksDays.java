package Models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

public class WorksDays implements Model {
    private String date;
    private ObjectsWork objectsWork;
    private Workers worker;
    private Integer hoursWorked;
    private String typeOfWork;

    public WorksDays () {
    }

    public WorksDays(String date) {
        this.date = date;
    }

    public WorksDays(int hoursWorked, String date, Workers worker) {
        this.hoursWorked = hoursWorked;
        this.date = date;
        this.worker = worker;
    }

    public WorksDays(int hoursWorked, String date, String typeOfWork, Workers worker) {
        this.typeOfWork = typeOfWork;
        this.hoursWorked = hoursWorked;
        this.date = date;
        this.worker = worker;
    }
    public WorksDays (String date, ObjectsWork objectsWork, Workers worker, String typeOfWork, int hoursWorked) {
        this.date = date;
        this.objectsWork = objectsWork;
        this.worker = worker;
        this.typeOfWork = typeOfWork;
        this.hoursWorked = hoursWorked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WorksDays worksDays = (WorksDays) o;
        return hoursWorked == worksDays.hoursWorked &&
                Objects.equals(typeOfWork, worksDays.typeOfWork) &&
                Objects.equals(date, worksDays.date) &&
                Objects.equals(worker, worksDays.worker);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hoursWorked, typeOfWork, date, worker);
    }

    public Integer getHoursWorked() {
        return hoursWorked;
    }

    public void setHoursWorked(int hoursWorked) {
        this.hoursWorked = hoursWorked;
    }

    public String getTypeOfWork() {
        return typeOfWork;
    }

    public void setTypeOfWork(String typeOfWork) {
        this.typeOfWork = typeOfWork;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Workers getWorker() {
        return worker;
    }

    public void setWorker(Workers worker) {
        this.worker = worker;
    }

    public void setObjectsWork(ObjectsWork objectsWork) {
        this.objectsWork = objectsWork;
    }

    public ObjectsWork getObjectsWork() {
        return objectsWork;
    }

    @Override
    public String getId() {
        return date + "|" + objectsWork.getName() + "|" + worker.getFullName();
    }
}
