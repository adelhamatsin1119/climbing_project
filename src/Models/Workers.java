package Models;

import java.util.ArrayList;
import java.util.Objects;

public class Workers implements Model{
    private long id;
    private String fullName;
    private ArrayList<ObjectsWork> workersObjects;
    private Integer workingHours;
    private Integer salary;



    public Workers () {
        this.workingHours = 0;
        this.workersObjects = new ArrayList<>();
        this.salary = 0;
    }
    public Workers (String fullName) {
        this.workingHours = 0;
        this.workersObjects = new ArrayList<>();
        this.fullName = fullName;
        this.salary = 0;
    }
    public Workers (String fullName,int workingHours) {
        this.workersObjects = new ArrayList<>();
        this.fullName = fullName;
        this.workingHours = workingHours;
        this.salary = 0;
    }


    public void setId(long id) {
        this.id = id;
    }

    public void setWorkerObject(ObjectsWork workObject) {
        if (workObject == null) {
            return;
        }
        if (workersObjects == null) {
            workersObjects = new ArrayList<>();
        }

        if (!workersObjects.isEmpty()) {

            for (ObjectsWork workObj: workersObjects) {
                if (workObj.getName().equals(workObject.getName()))
                    return;

                }
        }
        workersObjects.add(workObject);
    }

    public void setWorkersObjects(ArrayList<ObjectsWork> workersObjects)  {
        this.workersObjects = workersObjects;
    }
    public ArrayList<ObjectsWork> getWorkersObject() {
        if(workersObjects != null) {
            return workersObjects;
        } else {
            return null;
        }
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return this.fullName;
    }
    public void setWorkingHours (Integer workingHours) {
        if (this.workingHours > 0) {
            this.workingHours = this.workingHours + workingHours;
        } else {
            this.workingHours = workingHours;
        }
    }

    public Integer getWorkingHours() {
        return workingHours;
    }

    public void setSalary (Integer salary) {
        if (salary!= null) {
            try {
                this.salary = this.salary + salary;
            } catch (NullPointerException e) {
                this.salary = 0;
                this.salary = this.salary + salary;
            }
        }
    }

    public Integer getSalary() {
        return this.salary;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Workers workers = (Workers) o;
        return workingHours == workers.workingHours &&
                salary == workers.salary &&
                Objects.equals(fullName, workers.fullName) &&
                Objects.equals(workersObjects, workers.workersObjects);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fullName, workersObjects, workingHours, salary);
    }

    @Override
    public String getId() {
        return fullName;
    }
}
