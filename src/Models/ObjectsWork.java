package Models;

import java.util.ArrayList;
import java.util.StringJoiner;

public class ObjectsWork implements Model {
    private String name;
    private ArrayList<Workers> workers;


    public ObjectsWork() {
    }

    public ObjectsWork(String name) {
        this.name = name;
        this.workers = new ArrayList<>();
    }

    public ObjectsWork(String name, Workers worker) {
        this.name = name;
        this.workers = new ArrayList<>();
        this.workers.add(worker);
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setWorker(Workers worker) {

        if (worker == null) {
            return;
        }

        if (workers == null) {
            workers = new ArrayList<>();
        }

        if (!workers.isEmpty()) {
            for (Workers workers1 : workers) {
                if (workers1.getId().equals(worker.getId())) {
                    return;
                }
            }
        }
        workers.add(worker);
    }

    public ArrayList<Workers> getWorkers() {
        if (workers != null) {
            return workers;
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ObjectsWork.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .toString();
    }

    @Override
    public String getId() {
        return name;
    }
}
