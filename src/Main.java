import java.sql.Connection;
import java.sql.DriverManager;
import Repositories.*;
import Services.WorkServices;
import Services.WorkServicesImpl;

import java.sql.SQLException;
import java.util.regex.Pattern;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {


        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/ClimbingDB","postgres","qwerty007");
        }catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

        Pattern datePattern = Pattern.compile("\\d{4}-\\d{2}-\\d{2}");


        WorkersRepositories workersRepositories = new WorkersRepositoriesFileBasedImpl("Workers.txt");
        WorkObjectsRepositories workObjectsRepositories = new WorkObjectsRepositoriesFileBasedImpl("WorksObjects.txt");
        WorksDaysRepositories worksDaysRepositories = new WorksDaysRepositoriesFileBasedImpl("WorksDays.txt", workersRepositories, workObjectsRepositories);

        workersRepositories.setWorkObjectsRepositories(workObjectsRepositories);
        workersRepositories.setWorksDaysRepositories(worksDaysRepositories);

        workObjectsRepositories.setWorkersRepositories(workersRepositories);
        workObjectsRepositories.setWorksDaysRepositories(worksDaysRepositories);

        WorkServices workServices = new WorkServicesImpl(workersRepositories,workObjectsRepositories,worksDaysRepositories);
        Scanner scanner = new Scanner(System.in);


        while (true) {
            System.out.println("Здравствуйте введите ваше полное имя! Например(Федоров Виктор Николаевич)");
            String name = scanner.nextLine();

            if (workObjectsRepositories.allWorksObjects().size() > 0 ) {
                System.out.println("Выберите и введите название объекта из данного списка или введите новый -");
                workObjectsRepositories.allWorksObjects().forEach(objectsWork -> System.out.println(objectsWork.getId()));
            } else {
                System.out.println("Введите название нового объекта");
            }
            String nameObject = scanner.nextLine();
            System.out.println("Введите дату рабочей смены в формате ГГГГ-ММ-ДД (Например, 2024-12-21)");
            String date = scanner.nextLine();
            while(true){
                if(datePattern.matcher(date).matches()) {
                    break;
                } else {
                    System.out.println("Введенная дата не корректна! Пожалуйста, введите дату в формате ГГГГ-ММ-ДД (НАПРИМЕР, 2024-12-21)");
                    date = scanner.nextLine();
                }
            }

            System.out.println("Введите количество рабочих часов");
            int watch;
            while(true) {
                try{
                     watch = scanner.nextInt();
                    break;
                } catch(NumberFormatException e){
                    System.out.println("Введите количество часов целочисленным значением");
                }
            }

            scanner.nextLine();
            System.out.println("Введите область работ");
            String areaOfWork = scanner.nextLine();

            workServices.addingEntry(name, date,nameObject, watch, areaOfWork);

            System.out.println("Будут добавляться еще данные? Введите 'да' или 'нет'");
            
            String yerOrNot = scanner.nextLine();
            yerOrNot = yerOrNot.toLowerCase();
            if (yerOrNot.equals("нет")) {
                break;
            }
        }
    }
}