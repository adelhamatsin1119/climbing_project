import Models.Workers;
import Repositories.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MainTest {

    public static void main(String[] args) {

        /** подрубили драйвер
         try{
         Class.forName("org.postgresql.Driver");
         }catch(ClassNotFoundException e){
         throw new IllegalArgumentException(e);
         }
         **/


        Connection connection = null;

        try {
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/ClimbingDB","postgres","qwerty007");
        }catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

        WorkersRepositories workersRepositories = new WorkersRepositoriesJDBCImpl(connection);
        WorkersRepositories workersRepositories1 = new WorkersRepositoriesFileBasedImpl("Workers.txt");
        WorkObjectsRepositories workObjectsRepositories = new WorkObjectsRepositoriesFileBasedImpl("WorkObjects.txt");
        WorksDaysRepositories worksDaysRepositories = new WorksDaysRepositoriesFileBasedImpl("WorksDays.txt",workersRepositories,workObjectsRepositories);

        workersRepositories1.setWorkObjectsRepositories(workObjectsRepositories);
        workersRepositories1.setWorksDaysRepositories(worksDaysRepositories);

        workObjectsRepositories.setWorkersRepositories(workersRepositories);
        workObjectsRepositories.setWorksDaysRepositories(worksDaysRepositories);


        Workers workerDima = workersRepositories1.findByName("дима");
        workersRepositories.save(workerDima);

    }
}
