package Repositories;

public interface Mapper<X,Y> {
    Y map(X obj);

}
