package Repositories;

import Models.ObjectsWork;
import Models.Workers;
import Models.WorksDays;

import java.io.*;
import java.util.ArrayList;

import java.util.List;


public class WorksDaysRepositoriesFileBasedImpl implements WorksDaysRepositories {
    private final String fileName;
    private WorkersRepositories workersRepositories;
    private WorkObjectsRepositories workObjectsRepositories;
    private BufferedReader reader;
    private BufferedWriter writer;


    public WorksDaysRepositoriesFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    public WorksDaysRepositoriesFileBasedImpl(String fileName, WorkersRepositories workersRepositories,
                                              WorkObjectsRepositories workObjectsRepositories) {
        this.fileName = fileName;
        this.workersRepositories = workersRepositories;
        this.workObjectsRepositories = workObjectsRepositories;
    }


    public void setWorkersRepositories(WorkersRepositories workersRepositories) {
        this.workersRepositories = workersRepositories;
    }

    public void setWorkObjectsRepositories(WorkObjectsRepositories workObjectsRepositories) {
        this.workObjectsRepositories = workObjectsRepositories;
    }

    private final Mapper<WorksDays, String> toStringLineMapper = workDays -> {
        return workDays.getDate() +
                "|" +
                workDays.getObjectsWork().getId() +
                "|" +
                workDays.getWorker().getFullName() +
                "|" +
                workDays.getTypeOfWork() +
                "|" +
                workDays.getHoursWorked().toString() +
                "|";
    };
    private final Mapper<String, WorksDays> stringLineToWorkDayMapper = line -> {
        String[] parsedLine = line.split("\\|");
        WorksDays worksDays = new WorksDays();
        worksDays.setDate(parsedLine[0]);
        worksDays.setObjectsWork(new ObjectsWork(parsedLine[1]));
        worksDays.setWorker(new Workers(parsedLine[2]));
        worksDays.setTypeOfWork(parsedLine[3]);
        worksDays.setHoursWorked(Integer.parseInt(parsedLine[4]));
        return worksDays;
    };

    @Override
    public void save(WorksDays workDay) {
        try {
            //создаем ридер который считает всю инфу с файла
            reader = new BufferedReader(new FileReader(fileName));
            // создаем список и добавляем туда все с файла
            ArrayList<String> linesFromFile = new ArrayList<>(reader.lines().toList());
            // и добовляем еще и день который мы хотим сохранить, с помощью метода toStringLineMapper превращаем объект в текст
            linesFromFile.add(toStringLineMapper.map(workDay));
            // закрываем ридер
            reader.close();
            // создаем врайтер
            writer = new BufferedWriter(new FileWriter(fileName));
            //проходимся по всему списку
            linesFromFile.forEach(newWorkDay -> {
                try {
                    // и записываем каждый элемент списка в файл
                    writer.write(newWorkDay + "\n");
                } catch (IOException e) {
                    throw new IllegalArgumentException(e);
                }
            });
            writer.close();
        } catch (IOException e) {
            throw new IllegalArgumentException("GG");
        }
    }

    @Override
    public boolean containsByName(String infoDay) {
        //разделяем нашу информацию по дню
        String[] parsedInfoDay = infoDay.split("\\|");
        //создаем ридер чтобы считать все данные с файла
        try(BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            //считываем все данные с файла и обращаемся к методу anyMatch класса стрим
            return reader.lines().anyMatch(line -> {
                //по каждой считанной строке мы ее разделим на элементы там будет дата имя объекта и работника и т.д пользоваться будем только 3мя
                String[] parsedLine = line.split("\\|");
                //и будем возвращать true and false при выполнения данных условий это наш (ПРЕДИКАТ)
                return (parsedLine[0].equals(parsedInfoDay[0])
                        && parsedLine[1].equals(parsedInfoDay[1])
                        && parsedLine[2].equals(parsedInfoDay[2]));
                // СУТЬ МЕТОДА AnyMatch в том что как только выполняется условия ПРЕДИКАТА элемент кода выше
                // то сразу останавливает выполнение и возвращает true иначе если пройдет весь поток и не найдет true то он вернет
                // false

            });
            //ожидаемая ошибка
        } catch (IOException e) {
            //что делать при выбрасывании ошибки мы выбрасываем поверх не проверяемое исключение чтобы прервать работу програмы
           throw new IllegalArgumentException("Не найден файл " + fileName);
        }
    }

    @Override
    public WorksDays findByName(String infoDay) {
        //создаем ридер
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            //разделяем информацию по дню
            String[] parsedLine = infoDay.split("\\|");
            //создаем лист рабочих дней и с помощью ридера
            List<WorksDays> worksDaysArrayList = reader
                    //считываем все из файла
                    .lines()
                    //создаем рабочий день с помощью мап
                    .map(stringLineToWorkDayMapper::map)
                    // с помощью метода фильтр класса стрим
                    .filter(worksDays -> {
                        // проверяем на сходство рабочий день на разбитые элементы с входного параметра инфоДень
                       return worksDays.getDate().equals(parsedLine[0])
                               && worksDays.getObjectsWork().getName().equals(parsedLine[1])
                               && worksDays.getWorker().getFullName().equals(parsedLine[2]);
                       //переводим все это в лист там будет только наш нужный объект если он есть
                    }).toList();
            if (!worksDaysArrayList.isEmpty())
                //если он есть вернет его
                return worksDaysArrayList.get(0);
            else
                // иначе вернет нуль
                return null;


        }catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public void update(WorksDays worksDays) {
        //считать все с файла потом найти день с той же датой, что мы хотим обновить после заменить его на наш день и снова записать
        try {
            // создаем файлРидер
            reader = new BufferedReader(new FileReader(fileName));
            List<WorksDays> worksDaysArrayList = new ArrayList<>(reader.lines()
                    .map(stringLineToWorkDayMapper::map)
                    .toList());
            // закрываем ридер
            reader.close();
            //проходим по всему листу
            if (worksDaysArrayList.removeIf(worksDays1 -> worksDays1.equals(worksDays))) {
                worksDaysArrayList.add(worksDays);
            }
            writer = new BufferedWriter(new FileWriter(fileName));
            worksDaysArrayList.forEach(worksDays1 -> {
                try {
                    writer.write(toStringLineMapper.map(worksDays1) + "\n");
                } catch (IOException y) {
                    throw new RuntimeException();
                }
            });
            writer.close();

        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public ArrayList<WorksDays> getAllWorksDays() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {

            return new ArrayList<>(reader.lines()
                    .map(stringLineToWorkDayMapper::map)
                    .toList());
        } catch (IOException e) {
            throw new IllegalArgumentException("Failed to read file: " + fileName, e);
        }
    }
}
