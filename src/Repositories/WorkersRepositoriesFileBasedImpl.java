package Repositories;

import Models.ObjectsWork;
import Models.Workers;
import Models.WorksDays;

import java.util.List;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.IOException;
import java.util.Optional;


public class WorkersRepositoriesFileBasedImpl implements WorkersRepositories {
    private final String fileName;

    private WorkObjectsRepositories workObjectsRepositories;
    private WorksDaysRepositories worksDaysRepositories;

    private BufferedReader reader;
    private BufferedWriter writer;

    public WorkersRepositoriesFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    public WorkersRepositoriesFileBasedImpl (String fileName, WorkObjectsRepositories workObjectsRepositories,
                                             WorksDaysRepositories worksDaysRepositories) {
        this.fileName = fileName;
        this.workObjectsRepositories = workObjectsRepositories;
        this.worksDaysRepositories = worksDaysRepositories;
    }

    @Override
    public void setWorkObjectsRepositories(WorkObjectsRepositories workObjectsRepositories) {
        this.workObjectsRepositories = workObjectsRepositories;
    }
    @Override
    public void setWorksDaysRepositories(WorksDaysRepositories worksDaysRepositories) {
        this.worksDaysRepositories = worksDaysRepositories;
    }

    private Mapper<Workers,String> toStringLineMapper = worker -> {
        StringBuilder line = new StringBuilder();
        line.append(worker.getFullName())
                .append("|");
        if (worker.getWorkingHours() != null) {
            line.append(worker.getWorkingHours().toString())
                    .append("|");
        } else {
            line.append("NULL")
                    .append("|");
        }
        if(worker.getSalary() != null) {
            line.append(worker.getSalary().toString())
                    .append("|");
        } else {
            line.append("NULL")
                    .append("|");
        }

        return line.toString();
    };

    private Mapper<String,Workers> stringLineToWorkerMapper = line -> {
        Workers worker = new Workers();
        String[] parsedLine = line.split("\\|");
        worker.setFullName(parsedLine[0]);
        if (!parsedLine[1].equals("NULL")) {
            worker.setWorkingHours(Integer.parseInt(parsedLine[1]));
        } else {
            worker.setWorkingHours(null);
        }
        if (!parsedLine[2].equals("NULL")) {
            worker.setSalary(Integer.parseInt(parsedLine[2]));
        } else {
            worker.setSalary(null);
        }
        //добавим работнику из файла все его рабоиче объекты для этого пробежимся по всем рабочим дням
        if (!worksDaysRepositories.getAllWorksDays().isEmpty()) {

            for (WorksDays worksDays : worksDaysRepositories.getAllWorksDays()) {
                //и если мы будем находить нашего работника в списке рабочих дней
                if (worksDays.getWorker().getFullName().equals(worker.getFullName())) {
                    //то найдем объект из репозитория по рабочему дню и добавим его рабочему
                    worker.setWorkerObject(new ObjectsWork(worksDays.getObjectsWork().getId()));
                }
            }
        }
        return worker;
    };

    @Override
    public void save(Workers NewWorker) {
        try {
            reader = new BufferedReader(new FileReader(fileName));
           List<String> workersList = new java.util.ArrayList<>(reader.lines().toList());
           reader.close();
           workersList.add(toStringLineMapper.map(NewWorker));

           writer = new BufferedWriter(new FileWriter(fileName));

           workersList.forEach(worker -> {

               try {

                   writer.write(worker + "\n");

               } catch (IOException e) {
                   throw new IllegalArgumentException(e);
               }

           });
           writer.close();
        }catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public boolean containsByName(String FullName) {

        try(BufferedReader NewReader = new BufferedReader(new FileReader(fileName))) {
            return NewReader.lines().anyMatch(line -> {
                String[] parsedLine = line.split("\\|");
                return parsedLine[0].equals(FullName);
            });
        } catch (IOException e ){
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Workers findByName(String FullName) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            Optional<Workers> workerOptional = reader.lines().filter(line -> {
                String[] parsedLine = line.split("\\|");
               return parsedLine[0].equals(FullName);
            }).map(stringLineToWorkerMapper::map).findAny();
            return workerOptional.orElse(null);
        } catch (IOException e ) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Workers worker) {
        try{
            reader = new BufferedReader(new FileReader(fileName));
            List<String> lines = new java.util.ArrayList<>(reader.lines().toList());
            reader.close();
            lines.removeIf(line -> {
                String[] parsedLine = line.split("\\|");
               return parsedLine[0].equals(worker.getFullName());
            });
            lines.add(toStringLineMapper.map(worker));
            writer = new BufferedWriter(new FileWriter(fileName));
            lines.forEach(line -> {
                try {
                    writer.write(line + "\n");
                } catch (IOException e) {
                    throw new IllegalArgumentException(e);
                }
            });
            writer.close();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
