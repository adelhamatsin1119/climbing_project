package Repositories;

import Models.ObjectsWork;
import Models.Workers;

import java.util.List;
import java.util.ArrayList;

public class WorkersRepositoriesImplList implements WorkersRepositories {

    private List<Workers> workersList;

    public WorkersRepositoriesImplList() {
        this.workersList = new ArrayList<>();
    }

    @Override
    public void setWorkObjectsRepositories(WorkObjectsRepositories workObjectsRepositories) {

    }

    @Override
    public void setWorksDaysRepositories(WorksDaysRepositories worksDaysRepositories) {

    }

    @Override
    public void save(Workers worker) {
        workersList.add(worker);
    }

    @Override
    public boolean containsByName(String FullName) {
        for (Workers worker: workersList) {
            if (worker.getFullName().equals(FullName))
                return true;
        }
        return false;
    }

    @Override
    public Workers findByName(String FullName) {
        for (Workers worker: workersList) {
            if(worker.getFullName().equals(FullName))
                return worker;
        }
        return null;
    }

    @Override
    public void update(Workers newWorker) {
        workersList.removeIf(workers -> workers.getFullName().equals(newWorker.getFullName()));
                workersList.add(newWorker);
            }
}
