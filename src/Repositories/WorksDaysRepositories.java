package Repositories;
import Models.WorksDays;

import java.util.ArrayList;


public interface WorksDaysRepositories extends Repositories<WorksDays> {
    public void save (WorksDays workDay);
    public boolean containsByName(String infoDay);
    public WorksDays findByName(String infoDay);
    public void update (WorksDays worksDays);
    public ArrayList<WorksDays> getAllWorksDays();
}
