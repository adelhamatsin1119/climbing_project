package Repositories;

import Models.ObjectsWork;
import Models.Workers;

import java.util.ArrayList;
import java.util.List;

public class WorkObjectsRepositoriesImplList implements WorkObjectsRepositories {

    private List<ObjectsWork> worksObjects;
    public WorkObjectsRepositoriesImplList() {
        this.worksObjects = new ArrayList<>();
    }

    @Override
    public void setWorkersRepositories(WorkersRepositories workersRepositories) {

    }

    @Override
    public void setWorksDaysRepositories(WorksDaysRepositories worksDaysRepositories) {

    }

    @Override
    public void save(ObjectsWork workObjects) {
        this.worksObjects.add(workObjects);
    }
    @Override
    public boolean containsByName(String ObjectName) {
        for(ObjectsWork objects: worksObjects ) {
            if(objects.getName().equals(ObjectName))
                return true;
        }
        return false;
    }
    @Override
    public ObjectsWork findByName(String ObjectName) {
        for(ObjectsWork objects: worksObjects ) {
            if(objects.getName().equals(ObjectName))
                return objects;
        }
        return null;
    }
    @Override
    public List<ObjectsWork> allWorksObjects () {
       return this.worksObjects;
    }

    @Override
    public void update(ObjectsWork newWorkObject) {
        worksObjects.removeIf(objectsWork -> objectsWork.getId().equals(newWorkObject.getId()));
                worksObjects.add(newWorkObject);

            }
}
