package Repositories;

import Models.ObjectsWork;
import Models.Workers;
import Models.WorksDays;

import java.util.ArrayList;
import java.util.List;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileReader;
import java.util.Optional;

public class WorkObjectsRepositoriesFileBasedImpl implements WorkObjectsRepositories {
    private String fileName;

    private WorkersRepositories workersRepositories;
    private WorksDaysRepositories worksDaysRepositories;
    private BufferedReader reader;
    private BufferedWriter writer;


    public WorkObjectsRepositoriesFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    public WorkObjectsRepositoriesFileBasedImpl(String fileName, WorkersRepositories workersRepositories,
                                                WorksDaysRepositories worksDaysRepositories) {
        this.fileName = fileName;
        this.workersRepositories = workersRepositories;
        this.worksDaysRepositories = worksDaysRepositories;
    }

    @Override
    public void setWorkersRepositories(WorkersRepositories workersRepositories) {
        this.workersRepositories = workersRepositories;
    }
    @Override
    public void setWorksDaysRepositories(WorksDaysRepositories worksDaysRepositories) {
        this.worksDaysRepositories = worksDaysRepositories;
    }

    private Mapper<ObjectsWork, String> toStringLineMapper = objectsWork -> {
        StringBuilder line = new StringBuilder();
        line.append(objectsWork.getId())
                .append("|");

        return line.toString();
    };

    private Mapper<String, ObjectsWork> stringLineToObjectsWorkMapper = line -> {

        ObjectsWork objectsWork = new ObjectsWork();
        String[] parsedLine = line.split("\\|");
        objectsWork.setName(parsedLine[0]);

        if (!worksDaysRepositories.getAllWorksDays().isEmpty()) {

            for (WorksDays worksDay : worksDaysRepositories.getAllWorksDays()) {

                if (worksDay.getObjectsWork().getId().equals(objectsWork.getId())) {

                    objectsWork.setWorker(new Workers(worksDay.getWorker().getFullName()));
                }
            }
        }
        return objectsWork;
    };

    @Override
    public void save(ObjectsWork workObjects) {

        try {
            reader = new BufferedReader(new FileReader(fileName));
            List<String> lines = new java.util.ArrayList<>(reader.lines().toList());
            reader.close();
            lines.add(toStringLineMapper.map(workObjects));
            writer = new BufferedWriter(new FileWriter(fileName));
            lines.forEach(line -> {
                try {
                    writer.write(line + "\n");
                } catch (IOException e) {
                    throw new IllegalArgumentException(e);
                }
            });
            writer.close();
        } catch (IOException e) {
            throw new IllegalArgumentException("Файл не найден" + fileName);
        }
    }

    @Override
    public boolean containsByName(String objectName) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines().anyMatch(line -> {
                String[] parsedLine = line.split("\\|");
                return objectName.equals(parsedLine[0]);
            });
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }


    @Override
    public ObjectsWork findByName(String objectName) {

        //создаем ридер
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            //создаем контейнер Optional для хранения рабочего объекта
            Optional<ObjectsWork> optionalObjectsWork =
                    //считываем все строки с файла
                    reader.lines()
                            //преобразуем их в рабочие объекты
                            .map(stringLineToObjectsWorkMapper::map)
                            //во всем потоке находим объект по имени
                            .filter(objectsWork -> objectsWork.getName().equals(objectName))
                            //берем первый он точно должен быть там один и возвращаем его в виде объекта класса Optional
                            // который хранит в себе рабочий объект
                            .findFirst();

                return optionalObjectsWork.orElse(null);

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(ObjectsWork workObject) {
        try {
            // создаем ридер
            reader = new BufferedReader(new FileReader(fileName));
            // считываем с файла все строки и собираем из них с помощью функционального интерфейса коллекцию объектов
            List<ObjectsWork> objectsWorks = new ArrayList<>(reader.lines()
                    .map(stringLineToObjectsWorkMapper::map)
                    .toList());
            // закрываем ридер
            reader.close();

            //удаляем объект с тем же именем
            objectsWorks.removeIf(objectsWork -> objectsWork.getName().equals(workObject.getId()));
            //  и добавляем новый объект
            objectsWorks.add(workObject);
            // создаем врайтер
            writer = new BufferedWriter(new FileWriter(fileName));
            // проходимся по всему списку
            objectsWorks.forEach(objectsWork -> {
                try {
                    // и запишем каждый элемент списка сделав его строкой
                    writer.write(toStringLineMapper.map(objectsWork) + "\n");
                } catch (IOException a) {
                    throw new IllegalArgumentException(a);
                }
            });
            // закроем врайтер
            writer.close();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<ObjectsWork> allWorksObjects() {
        try (BufferedReader reader1 = new BufferedReader(new FileReader(fileName))) {

            return new ArrayList<>(reader1.lines().map(stringLineToObjectsWorkMapper::map).toList());

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
