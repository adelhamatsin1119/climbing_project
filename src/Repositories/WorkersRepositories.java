package Repositories;

import Models.ObjectsWork;
import Models.Workers;

public interface WorkersRepositories extends Repositories<Workers>   {
    public void setWorkObjectsRepositories(WorkObjectsRepositories workObjectsRepositories);

    void setWorksDaysRepositories(WorksDaysRepositories worksDaysRepositories);

    public void save (Workers worker);
    public boolean containsByName(String FullName);
    public Workers findByName(String FullName);
    public void update (Workers worker);
}
