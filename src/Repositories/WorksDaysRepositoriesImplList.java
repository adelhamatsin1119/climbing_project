package Repositories;

import Models.WorksDays;

import java.util.List;
import java.util.ArrayList;

public class WorksDaysRepositoriesImplList implements WorksDaysRepositories {
    private List<WorksDays> worksDays;

    public WorksDaysRepositoriesImplList(String s) {
        this.worksDays = new ArrayList<>();
    }

    @Override
    public void save(WorksDays workDay) {
        this.worksDays.add(workDay);
    }

    @Override
    public boolean containsByName(String infoDay) {
        for (WorksDays workDay : worksDays) {
            if (workDay.getId().equals(infoDay))
                return true;
        }
        return false;
    }

    @Override
    public WorksDays findByName(String infoDay) {
        for (WorksDays workDay : worksDays) {
            if (workDay.getId().equals(infoDay))
                return workDay;
        }
        return null;
    }

    @Override
    public void update(WorksDays workDay) {
        worksDays.removeIf(worksDays1 -> worksDays1.getId().equals(workDay.getId()));
        worksDays.add(workDay);
    }

    @Override
    public ArrayList<WorksDays> getAllWorksDays() {
        return null;
    }
}
