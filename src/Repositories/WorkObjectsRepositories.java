package Repositories;

import Models.ObjectsWork;

import java.util.List;

public interface WorkObjectsRepositories extends Repositories<ObjectsWork>{
    public void setWorkersRepositories(WorkersRepositories workersRepositories);

    void setWorksDaysRepositories(WorksDaysRepositories worksDaysRepositories);

    public void save (ObjectsWork workObjects);
    public boolean containsByName(String ObjectName);
    public ObjectsWork findByName(String ObjectName);
    public void update (ObjectsWork workObject);
    public List<ObjectsWork> allWorksObjects ();

}
