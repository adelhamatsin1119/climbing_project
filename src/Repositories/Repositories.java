package Repositories;

import Models.Workers;

public interface Repositories<X> {
    public void save (X x);
    public boolean containsByName(String FullName);
    public X findByName(String FullName);
    public void update (X x);
}
