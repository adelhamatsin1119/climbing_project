package Repositories;

import Models.Workers;

import java.sql.*;

public class WorkersRepositoriesJDBCImpl implements WorkersRepositories {

    Connection connection;

    //Language=SQL
    private static final String SQL_INSERT = "insert into workers(full_name,working_hours,salary) values(?,?,?);";

    public WorkersRepositoriesJDBCImpl (Connection connection) {
        this.connection = connection;
    }

    @Override
    public void setWorkObjectsRepositories(WorkObjectsRepositories workObjectsRepositories) {

    }

    @Override
    public void setWorksDaysRepositories(WorksDaysRepositories worksDaysRepositories) {

    }

    @Override
    public void save(Workers worker) {
        PreparedStatement preparedStatement = null;
        ResultSet generatedKeys = null;

        try {
            preparedStatement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, worker.getFullName());
            preparedStatement.setInt(2,worker.getWorkingHours());
            preparedStatement.setInt(3,worker.getSalary());
            int affectedRows = preparedStatement.executeUpdate();

            if(affectedRows != 1 ) {
                throw new SQLException("Can`t insert");
            }
            generatedKeys = preparedStatement.getGeneratedKeys();
            if(generatedKeys.next()) {
                worker.setId(generatedKeys.getInt("id"));
            } else {
                throw new SQLException("Can`t retrieve id");
            }

        }catch(SQLException e) {
            throw new IllegalStateException(e);
        }finally{

            if(generatedKeys != null) {
                try {
                    generatedKeys.close();
                }catch (SQLException throwables) {
                    //ignore
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException throwables) {
                    //ignore
                }
            }
        }
    }

    @Override
    public boolean containsByName(String FullName) {
        return false;
    }

    @Override
    public Workers findByName(String FullName) {
        return null;
    }

    @Override
    public void update(Workers worker) {

    }
}
